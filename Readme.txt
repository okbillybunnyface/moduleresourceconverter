RESOURCECONVERTER v1.2 - okbillybunnyface
Source Code: https://bitbucket.org/okbillybunnyface/moduleresourceconverter/src/

----------------------------------------
This is a simple plugin to do resource conversion in a part.

INSTALLATION:
Simply copy the contents ResourceConverter1.2.zip into your KSP directory, and make the necessary additions to the part's part.cfg (detailed below).

MODULE
{
 name = ModuleResourceConverter
 canOverflow = false		//--optional, defaults to false (If set to true, the converter will continue to operate, consuming input resources, even if one of the outputs is full.)
 deactivateIfFull = false           //--optional, defaults to false (If set to true, the converter automatically deactivates if one of the output resources is full.)
 deactivateIfEmpty = false      //--optional, defaults to false (If set to true, the converter automatically deactivates if there one of the input resources is missing. If false, it will continue to 'operate' at 0 speed)
 INPUT_RESOURCE
 {
    name = ElectricCharge
    rate = 1
 }
 INPUT_RESOURCE
 {
    name = LiquidFuel
    rate = 1
 }
 OUTPUT_RESOURCE
 {
    name = MonoPropellant
    rate = 1
 }
 OUTPUT_RESOURCE
 {
    name = Oxidizer
    rate = 1
 }
}

Changelog:
v1.2
-Wrote a (much) better version of the stock part.RequestResource() method. Like, there's no comparison.
-Rewrote the entire Convert() method to be more robust and to make use of the new RequestResource() method.

v1.1
-Added a status field to converters.
-Added canOverflow functionality
-Fixed bug with resource flow modes (hopefully, seemed to work)

License: Feel free to use or edit this however you want to. You can credit me if you like, but it is not necessary.
