﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


class ModuleResourceConverter : PartModule
{
    [KSPField(isPersistant = true, guiActive = true, guiName = "Converter Status")]
    public string converterStatus = "Deactivated";

    [KSPField(isPersistant = true)]
    public bool converterIsActive = false;

    [KSPField(isPersistant = true)]
    public bool canOverflow = false;

    [KSPField(isPersistant = true)]
    public bool deactivateIfFull = false;

    [KSPField(isPersistant = true)]
    public bool deactivateIfEmpty = false;

    private bool overflowing = false, underflowing = false;

    private double minTransfer = 0.0001;
    private double maxSeconds = 20;
    private bool foundResources = false;

    public override string GetInfo()
    {
        String info = "Can Overflow: " + canOverflow + "\n";
        info = String.Concat(info, "Deactivates if full: " + deactivateIfFull + "\n");
        info = String.Concat(info, "Deactivates if empty: " + deactivateIfEmpty + "\n");

        info = String.Concat(info, "\nInput Resources:\n");
        foreach (ModuleGenerator.GeneratorResource resource in inputList)
        {
            info = String.Concat(info, resource.name + " (" + resource.rate + " units/second)\n");
        }


        info = String.Concat(info, "\nOutput Resources:\n");
        foreach (ModuleGenerator.GeneratorResource resource in outputList)
        {
            info = String.Concat(info, resource.name + " (" + resource.rate + " units/second)\n");
        }

        return info;
    }

    private ResourceTracker[] inputResources, outputResources;
    public List<ModuleGenerator.GeneratorResource> inputList = new List<ModuleGenerator.GeneratorResource>();
    public List<ModuleGenerator.GeneratorResource> outputList = new List<ModuleGenerator.GeneratorResource>();

    public override void OnLoad(ConfigNode node)
    {
        inputList.Clear();
        outputList.Clear();

        if (node.HasValue("canOverflow"))
        {
            canOverflow = Boolean.Parse(node.GetValue("canOverflow"));
        }
        if (node.HasValue("deactivateIfFull"))
        {
            deactivateIfFull = Boolean.Parse(node.GetValue("deactivateIfFull"));
        }
        if (node.HasValue("deactivateIfEmpty"))
        {
            deactivateIfEmpty = Boolean.Parse(node.GetValue("deactivateIfEmpty"));
        }

        foreach (ConfigNode subNode in node.nodes)
        {
            ModuleGenerator.GeneratorResource stuff;
            switch (subNode.name)
            {
                case "INPUT_RESOURCE":
                    stuff = new ModuleGenerator.GeneratorResource();
                    stuff.Load(subNode);
                    inputList.Add(stuff);
                    break;
                case "OUTPUT_RESOURCE":
                    stuff = new ModuleGenerator.GeneratorResource();
                    stuff.Load(subNode);
                    outputList.Add(stuff);
                    break;
            }
        }

        inputResources = new ResourceTracker[inputList.Count];
        int temp = 0;
        foreach (ModuleGenerator.GeneratorResource resource in inputList)
        {
            inputResources[temp] = new ResourceTracker(resource.name, resource.id, resource.rate, this.part);
            temp++;
        }

        outputResources = new ResourceTracker[outputList.Count];
        temp = 0;
        foreach (ModuleGenerator.GeneratorResource resource in outputList)
        {
            outputResources[temp] = new ResourceTracker(resource.name, resource.id, resource.rate, this.part);
            temp++;
        }
    }

    public override void OnSave(ConfigNode node)
    {
        node.AddValue("canOverflow", canOverflow);
        node.AddValue("deactivateIfFull", deactivateIfFull);
        node.AddValue("deactivateIfEmpty", deactivateIfEmpty);
        foreach (ModuleGenerator.GeneratorResource resource in inputList)
        {
            resource.Save(node.AddNode("INPUT_RESOURCE"));
        }
        foreach (ModuleGenerator.GeneratorResource resource in outputList)
        {
            resource.Save(node.AddNode("OUTPUT_RESOURCE"));
        }
    }

    public override void OnStart(StartState state)
    {
        StartCoroutine(GetStatus());
        Events["Activate"].active = true;
        Events["Deactivate"].active = false;
    }

    public override void OnAwake()
    {
        if (inputList == null)
        {
            inputList = new List<ModuleGenerator.GeneratorResource>();
        }
        if (outputList == null)
        {
            outputList = new List<ModuleGenerator.GeneratorResource>();
        }
    }

    double gameTime = Planetarium.GetUniversalTime();
    public void FixedUpdate()
    {
        double deltaTime = Planetarium.GetUniversalTime() - gameTime;
        gameTime = Planetarium.GetUniversalTime();

        if (converterIsActive) Convert(deltaTime);
        else
        {
            underflowing = false;
            overflowing = false;
        }
    }
    
    public void Convert(double deltaTime)
    {
        if (overflowing && !canOverflow)
        {
            overflowing = false;
            foreach (ResourceTracker resource in outputResources)
            {
                resource.UpdateResources();

                if (resource.vesselMax - resource.vesselAmount < 0.001)
                {
                    overflowing = true;
                    break;
                }
            }
        }
        else
        {
            double efficiency = 1;

            //Fetch all the input resources from the vessel and calculate the efficiency this cycle
            foreach (ResourceTracker resource in inputResources)
            {
                resource.request = resource.rate * deltaTime;
                resource.FetchResource(resource.request);
                double ratio = resource.amount / resource.request;
                if (ratio < efficiency) efficiency = ratio;
            }

            underflowing = (efficiency < 0.9);
            if (underflowing && !overflowing) converterStatus = "Underflowing!";
            if (efficiency == 0 && deactivateIfEmpty) Deactivate();//Out of something! Get more! (The converter stops running if there is none of a reagent anyway)

            //Convert the input resources to output resources based on the efficiency
            foreach (ResourceTracker resource in inputResources)
            {
                resource.amount -= resource.request * efficiency;
                if (resource.amount < 0) resource.amount = 0;//only in case of rounding error shenanigans
            }
            foreach (ResourceTracker resource in outputResources)
            {
                resource.amount += resource.rate * deltaTime * efficiency;
            }

            //Deliver all output resources as well as any input resources that remain back to the vessel
            foreach (ResourceTracker resource in inputResources)
            {
                resource.DeliverResource();
            }
            foreach (ResourceTracker resource in outputResources)
            {
                resource.DeliverResource();
                if (resource.amount > 0.001)
                {
                    overflowing = true;
                    converterStatus = "Overflowing!";
                    resource.amount = 0;
                    if (deactivateIfFull) Deactivate();
                }
            }
        }
    }
    
    IEnumerator GetStatus()
    {
        while (true)
        {
            if (overflowing)
            {
                converterStatus = "Overflowing!";
            }
            else if (underflowing)
            {
                converterStatus = "Underflowing!";
            }
            else if (converterIsActive)
            {
                converterStatus = "Nominal";
            }
            else converterStatus = "Deactivated";

            yield return new WaitForSeconds(0.5f);
        }
    }

    [KSPAction("Activate Converter")]
    public void ActivateAction(KSPActionParam param)
    {
        Activate();
    }

    [KSPAction("Shutdown Converter")]
    public void ShutdownAction(KSPActionParam param)
    {
        Deactivate();
    }

    [KSPAction("Toggle Converter")]
    public void ToggleAction(KSPActionParam param)
    {
        if (converterIsActive)
        {
            Deactivate();
        }
        else
        {
            Activate();
        }
    }

    [KSPEvent(guiActive = true, guiName = "Activate Converter", active = true, guiActiveUnfocused = false, guiActiveEditor = false, externalToEVAOnly = true)]
    public void Activate()
    {
        Events["Activate"].active = false;
        Events["Deactivate"].active = true;

        converterStatus = "Nominal";
        converterIsActive = true;
    }

    [KSPEvent(guiActive = true, guiName = "Deactivate Converter", active = false, guiActiveUnfocused = false, guiActiveEditor = false, externalToEVAOnly = true)]
    public void Deactivate()
    {
        Events["Activate"].active = true;
        Events["Deactivate"].active = false;

        converterStatus = "Deactivated";
        converterIsActive = false;
    }

    // An object to keep track of a resource that is capable of importing and exporting it from the vessel at large.
    public class ResourceTracker
    {
        public int id;
        public double rate, amount, request, vesselAmount, vesselMax;
        private Part part;
        public List<PartResource> sources;

        public ResourceTracker(string name, int id, double rate, Part part)
        {
            this.id = id;
            this.rate = rate;
            this.amount = 0;
            this.request = 0;
            this.vesselAmount = 0;
            this.vesselMax = 0;
            this.part = part;
            this.sources = new List<PartResource>();
        }

        //Recompiles the list of resources on the vessel.
        public void UpdateResources()
        {
            part.GetConnectedResources(id, ResourceFlowMode.ALL_VESSEL, sources);
            vesselAmount = 0;
            vesselMax = 0;
            foreach (PartResource partResource in sources)
            {
                if (partResource.amount < 0) partResource.amount = 0;
                else if (partResource.amount > partResource.maxAmount) partResource.amount = partResource.maxAmount;
                vesselAmount += partResource.amount;
                vesselMax += partResource.maxAmount;
            }
        }

        //Makes use of RequestResource to fetch an amount (or what is available) of a resource from the vessel and add it to the local class variable 'amount'
        public void FetchResource(double amount)
        {
            if (amount > 0) this.amount += RequestResource(amount);
        }

        //Attempts to deliver all of the local amount to the vessel
        public void DeliverResource()
        {
            this.amount += RequestResource(-this.amount);
            if (this.amount < 0) this.amount = 0;
        }

        /*Gives or takes an amount of a resource from the vessel and returns the amount transferred. A negative amount will send resources back to the vessel.
         * This method can be used generically with other mods as long as the mod makes a ResourceTracker object, which is used to keep track of which resource is being transferred. 
         * It doesn't edit any ResourceTracker class variables. */
        public double RequestResource(double amount)
        {
            UpdateResources();

            if (amount == 0) return 0;
                /*
            else if (amount > 0)
            {
                if (vesselAmount <= 0) return 0;
            }
            else if (vesselAmount >= vesselMax) return 0;   */

            //A List that is used to keep track of the resource sources on the vessel that have available resources/space
            List<PartResource> resourceList = new List<PartResource>();

            //Keeps track of final output
            double output = 0;

            //This loop adds the relevant PartResource objects to the resourceList
            foreach (PartResource resource in sources)
            {
                double available = (amount >= 0) ? (resource.amount) : (resource.maxAmount - resource.amount);
                if (available > 0) resourceList.Add(resource);
            }

            //Divides the amount parameter by the number of useable resource sources
            double individualAmount = amount / resourceList.Count;

            //This loop iterates through the useable resource sources and makes the transfers
            foreach (PartResource resource in resourceList)
            {
                //This is either the amount of resource or the amount of space available, depending on whther resources are being sent or retrieved
                double available = (amount >= 0) ? (resource.amount) : (resource.maxAmount - resource.amount);

                //If there is enough resource/space, then
                if( available >= Math.Abs(individualAmount) )
                {
                    //Subtract the desired amount and add it to the final output
                    resource.amount -= individualAmount;
                    output += individualAmount;
                }
                //Otherwise
                else
                {
                    double temp = individualAmount - available;
                    //Add the available resource/space to the final output
                    output += resource.amount;
                    //Set the available resource/space to 0/the max amount.
                    resource.amount = (amount >= 0) ? 0 : resource.maxAmount;
                    //Call this method again on the unfulfilled amount, and add the return to the final output (the newly empty/full source won't be considered on the next pass)
                    output += RequestResource(temp);
                }
            }

            return output;
        }
    }
}